select
category, 
percentile_disc(0.5) within group (order by age) as age_median,
avg (age ) as age_avg
from
organization_view inner join organization_region on organization_region.id =   organization_view.region_id
inner join organization_organizationcategory on organization_organizationcategory.id = organization_view.category_id
inner join organization_size on organization_size.id = organization_view.size_id
where 
category_id is not null
[[ and {{region}} ]]
[[ and {{category}} ]]
[[ and {{size}} ]]
group by category
order by category