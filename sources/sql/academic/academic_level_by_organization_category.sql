Select 
organization_size,
category, 
academic_category,
count (*) as quantidade
from employee_academic_category_view e_v 
inner join organization_view o_v on e_v.organization_id = o_v.organization_id 
group by organization_size, category, academic_category
order by category, academic_category