
# Dashboard

## 🚀 Goal

We create a dashboard to visualize the data present in the application. To install the application you need to execute the following command:

## ⚙️ Requirements

1. Docker Compose

## 💻 Usage

```
docker-compose up -d 
```

## 🛠️ Stack

1. Metabase


## ✒️ Team


## 📕 Literature













```bash

docker run -d -p 3000:3000 -name zeppelindash nemoufes/zeppelindashboard:version3
```

The following credential could be used to enter in the dashboard:

**User**: zepplein@nemo.ifes.edu.br

**password**: nemo2022

d